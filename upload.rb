#!/usr/bin/env ruby
require 'rest-client'

NEW_TASK_URL = 'http://localhost:9000/tasks'

unless ARGV.length == 2
  STDERR.puts 'Usage: ./upload <first_id> <last_id>'
  exit 1
end

first = ARGV[0].to_i
last = ARGV[1].to_i

begin
  first.upto(last) do |id|
    t = { id: id.to_s }
    RestClient.post NEW_TASK_URL, t.to_json, content_type: :json
    puts "Task added!"
  end
rescue => e
  puts "Failed to add task: #{e.response}"
end
