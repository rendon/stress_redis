package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	"io/ioutil"
	"log"
	"menteslibres.net/gosexy/redis"
	"net/http"
)

var rc *redis.Client

func init() {
	rc = redis.New()
	if err := rc.Connect("redis-server", 6379); err != nil {
		log.Fatalf("Couldn't connect to Redis server")
	}
}

type Task struct {
	Id string `json:"id"`
}

type SuccessResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type ErrorResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
	return c.Render()
}

func (c App) Error(e error) revel.Result {
	c.Response.Status = http.StatusBadRequest
	c.Response.ContentType = "application/json"

	var res = ErrorResponse{
		Status:  http.StatusBadRequest,
		Message: e.Error(),
	}

	log.Printf("Error: %s", e)
	return c.RenderJson(res)
}

func (c App) Ok() revel.Result {
	c.Response.Status = http.StatusOK
	c.Response.ContentType = "application/json"

	var res = SuccessResponse{
		Status:  http.StatusOK,
		Message: http.StatusText(http.StatusOK),
	}
	log.Printf("Ok!")
	return c.RenderJson(res)
}

func (c App) CreateTask() revel.Result {
	var err error
	var body []byte
	if body, err = ioutil.ReadAll(c.Request.Body); err != nil {
		return c.Error(err)
	}
	var t Task
	if err = json.Unmarshal(body, &t); err != nil {
		return c.Error(err)
	}

	if value, err := rc.HGet("explored", t.Id); err == nil && value == "1" {
		return c.Error(fmt.Errorf("Already explored."))
	}

	if _, err = rc.HSet("explored", t.Id, "1"); err != nil {
		return c.Error(fmt.Errorf("Error marking task as explored: %s", err))
	}

	return c.Ok()
}
